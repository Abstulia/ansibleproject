import tkinter as tk
import os
from tkinter import ttk
import subprocess as sb
from tkinter import scrolledtext
from tkinter import Image
from PIL import Image,ImageTk
#import colorama

my_env = os.environ.copy()
my_env['MYPY_FORCE_COLOR'] = '3'

playBookSelect = []
pcSelect = []
configPc = []
ip = []
playbook = []
listCommande = []
#colorama.init()

def boutonValiderPC(list,parent):
    pcSelect = []
    for i in range(len(list)):
         pcSelect.append(list[i].get())
   # print(pcSelect)
    fichier = open("/home/cyberlab/ansibleproject/inventory/hosts.ini", "w")
    fichier.write("\n")
    fichier.write(line[1] + "\n")
    fichier.write("\n")

    for k in range(0, len(pcSelect)):
        if(pcSelect[k] == 1):
                fichier.write(line[k + 3] + "\n")

    fichier.close()
    parent.destroy()

def ttSelec(listButton,listPC):
	nombreDeCoche=0
	for i in range (len(listButton)):
		nombreDeCoche+=listPC[i].get()
	if(nombreDeCoche<len(listButton)):
		for i in range (len(listButton)):
			listButton[i].select()
	else:
		for i in range (len(listButton)):
			listButton[i].deselect()
		

def boutonValiderPlayBook(list, parent):
    playBookSelect = []

    fichier = open("commande.txt", "w")
    for i in range(len(list)):
        playBookSelect.append(list[i].get())
    for k in range(0, len(playBookSelect)):
        if (playBookSelect[k] == 1):
            fichier.write(listCommande[k] +"\n")
    fichier.close()
    parent.destroy()
   

def menuDeroulantPC():
    window2 = tk.Toplevel()
    scrollbar = tk.Scrollbar(window2)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    checklist = tk.Text(window2, width=20)
    checklist.pack()
	
    listCheckButton=[]
    listPC = []
    for i in range(len(ip)):
        var = tk.IntVar()
        listPC.append(var)
        txt = "host" + str(i + 1) + ": " + ip[i]
        checkbutton = tk.Checkbutton(checklist, text=txt, variable=var)
        listCheckButton.append(checkbutton)
        checklist.window_create("end", window=checkbutton)
        checklist.insert("end", "\n")
        

    checklist.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=checklist.yview)
    

    # disable the widget so users can't insert text into it
    checklist.configure(state="disabled")
    ttSelecButton=tk.Button(window2,text= "Tout sélectionner",command=lambda:ttSelec(listCheckButton,listPC))
    ttSelecButton.pack()
    valider = tk.Button(window2, text="Valider", command=lambda: boutonValiderPC(listPC, window2))
    valider.pack()


def menuDeroulantPlayBook():
    window2 = tk.Toplevel()
    scrollbar = tk.Scrollbar(window2)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    checklist = tk.Text(window2, width=20)
    checklist.pack()

    listPlayBook = []
    for i in range(len(playbook)):
        var = tk.IntVar()
        listPlayBook.append(var)
        txt = "host" + str(i + 1) + ": " + playbook[i]
        checkbutton = tk.Checkbutton(checklist, text=txt, variable=var)
        checklist.window_create("end", window=checkbutton)
        checklist.insert("end", "\n")

    checklist.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=checklist.yview)

    # disable the widget so users can't insert text into it
    checklist.configure(state="disabled")
    valider = tk.Button(window2, text="Valider", command=lambda: boutonValiderPlayBook(listPlayBook, window2))
    valider.pack()

def bash():
    fichier = open("commande.txt", "r")
    lines = fichier.readlines()


    for i in range (len(lines)):
        listeCommande = lines[i].strip().split(" ")
        #print(listeCommande)

        # Run the terminal command and store the output
        output = sb.run(listeCommande, capture_output=True ,env=my_env);

        # Insert the output into the scrolledtext widget
        scrolled_text.insert("end", output.stdout)#.decode())


with open("/home/cyberlab/ansibleproject/inventory/list_hosts.ini", "r") as file:
    line = file.read().splitlines()

for i in range(3, len(line)):
    configPc.append(line[i].split(" "))

for j in range(0, len(configPc)):
    ipAd = configPc[j][1].split('=')
    ip.append(ipAd[1])

with open("playbook.txt", "r") as file2:
    lines = file2.read().splitlines()

for i in range(0, len(lines)):
    x = lines[i].split("*")
    listCommande.append(x[0])
    playbook.append(x[1])


window = tk.Tk()
window.geometry("600x600")

iconAnsible=Image.open("/home/cyberlab/ansibleproject/scripts/ansible.png")
iconAnsible=iconAnsible.resize((20,20))
iconAnsible=ImageTk.PhotoImage(iconAnsible)

iconPC=Image.open("/home/cyberlab/ansibleproject/scripts/iconPC.png")
iconPC=iconPC.resize((20,20))
iconPC=ImageTk.PhotoImage(iconPC)

buttonMenuDeroulantPC = tk.Button(window, image = iconPC ,compound="left",text="liste PC", command=menuDeroulantPC)
#buttonMenuDeroulantPC.pack(side="top")
buttonMenuDeroulantPC.grid(column=1,row=0)
buttonMenuDeroulantPlayBook = tk.Button(window,image=iconAnsible,compound="left", text="liste PlayBook", command=menuDeroulantPlayBook)
#buttonMenuDeroulantPlayBook.pack(side="top")
buttonMenuDeroulantPlayBook.grid(column=3,row=0)

buttonBashRun = tk.Button(window, text="Run", command=bash)
#buttonBashRun.pack()
buttonBashRun.grid(column=2,row=3)

scrolled_text = tk.scrolledtext.ScrolledText(window)
#scrolled_text.pack()
scrolled_text.grid(columnspan=4,rowspan=6)









window.mainloop()

